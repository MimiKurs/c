﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mag_kvadrat_1._2
{
    public class KvadratoRazdavatel
    {
        public static void take(ref int[,] massiv)
        {
            int razmer = Convert.ToInt16(Math.Sqrt(massiv.Length));
            if (razmer == 3)
            {
                massiv[0, 0] = 4;
                massiv[0, 1] = 9;
                massiv[0, 2] = 2;
                massiv[1, 0] = 3;
                massiv[1, 1] = 5;
                massiv[1, 2] = 7;
                massiv[2, 0] = 8;
                massiv[2, 1] = 1;
                massiv[2, 2] = 6;
            }
           //нечетные квадраты 5 и больше разм
            int a=0, b=0,im=0,jm=(razmer-1)/2,schetchik=1,zapolnie=0;//буффера счетчик заполнения
            int r = razmer - 1;
           /*( for (int i = 0; i < razmer; i++)
                for (int j = 0; j < razmer; j++)
                    massiv[i, j] = 0;
            */
            if (razmer >=5&&razmer%2!=0)
            { 
                 int i=0, j=Convert.ToInt16(razmer/2), cshet=0;
                 massiv[i, j] = 1;
                 i--;
                 j++;
                 schetchik++;

                do
                {
                 

                   
                      if (i < razmer && j < razmer && i >= 0&&j>=0)
                    {

                        if (i == 0 && j == razmer - 1)
                        { massiv[i, j] = schetchik;
                        schetchik++;
                        i++;
                        }
                        else if (massiv[i, j] == 0)
                        {
                            massiv[i, j] = schetchik;
                            schetchik++;
                            i--;
                            j++;
                        }


                        else if (massiv[i, j] != 0)
                        {
                            i += 2;
                            j--;
                            massiv[i, j] = schetchik;
                            schetchik++;
                            i--;
                            j++;

                        }
                    }
                      
                   
                   else if (i < 0)
                    { i = razmer - 1; }
                   else if ( j > razmer - 1)
                        j = 0;



                }while(schetchik!=(razmer*razmer)+1);

                 
                
            }
            else if (razmer >= 4 && razmer % 2 == 0)
            {
                int[] masBuf = new int[razmer];
                for (int i = 0; i < razmer; i++)
                    for (int j = 0; j < razmer; j++)
                    { a++; massiv[i, j] = a; }
                //первая диагональ
                for (int i = 0; i < razmer; i++)
                    for (int j = 0; j < razmer; j++)
                        if (i == j)
                            masBuf[i] = massiv[i, j];
                for (int i = 0; i < razmer; i++)
                    for (int j = 0; j < razmer; j++)
                        if (i == j)
                        {
                            massiv[i, j] = masBuf[r];
                            r--;
                        }
                //другая диагональ
                r = razmer - 1;
                for (int i = 0; i < razmer; i++)
                    for (int j = 0; j < razmer; j++)
                        if (i + j == r)
                            masBuf[i] = massiv[i, j];

                for (int j = 0; j < razmer; j++)
                    for (int i = r; i > -1; i--)

                        if (i + j == r)
                        {
                            massiv[i, j] = masBuf[j];
                          
                        }

            }
               
        }
    }
    
}