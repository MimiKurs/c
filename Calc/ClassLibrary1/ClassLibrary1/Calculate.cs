﻿
namespace Calc
{
    public static class Calculate
    {
        static double plus(double a, double b)
        {
            double result;
            result = a + b;
            return result;
        }

        static double minus(double a, double b)
        {
            double result;
            result = a - b;
            return result;
        }

        static double divide(double a, double b)
        {
            double result;
            result = a / b;
            return result;
        }

        static double multiply(double a, double b)
        {
            double result;
            result = a * b;
            return result;
        }


    }
}
