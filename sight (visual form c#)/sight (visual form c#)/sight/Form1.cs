﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace sight
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static Pen pn;
        static Brush br;
        static Graphics ph;
       static Random rnd = new Random();
        static int x, y,r=25,x0=0;
        Thread atack = new Thread(attc);
        Thread decr = new Thread(decrace);
        Thread shots = new Thread(shot);
        static int[,] massiv=new int[100,2];
       
        private void Form1_Load(object sender, EventArgs e)
        {  
        //----- points
     
            //---points end
            ph = this.CreateGraphics();
            pn = new Pen(Color.Black, 1);
            br = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            
            DoubleBuffered = true;
            this.Cursor.Dispose();
            atack.Start();
            atack.Suspend();
            shots.Start();
            shots.Suspend();
            decr.Start();
            for (int i = 0; i < 100; i++)
                for (int j = 0; j < 2; j++)
                    massiv[i, j] = 1;
           
        }
        static void attc()
        {
           
            while (true)
            {
               
                if (x0 < 25)
                {
                    
                    x0++;
                    draw();
                 
                   
                }
                Thread.Sleep(10);
            }

        }


        static void shot()
        {
            try
            {
                while (true)
                {
                    int xR = rnd.Next(x-x0, x + r+x0);
                    int yR = rnd.Next(y-x0, y + r+x0);
                    massiv[0, 0] = xR;
                    massiv[0, 1] = yR;
                    for (int i = 98; i > 0; i--)
                        for (int j = 0; j < 2; j++)
                        { massiv[i, j] = massiv[i - 1, j]; }
                    Thread.Sleep(100);
                }
        }
        catch
        { }
        }
         

        static void draw()
        {
          try {
         
            ph.Clear(Color.White);

          
            x = Cursor.Position.X;
            y = Cursor.Position.Y;
           // pt[0] = ; //1
         
            ph.DrawLine(pn, new Point(x+r/2,y-x0),new Point(x+r/2,y+r/4-x0)); //1
            ph.DrawLine(pn, new Point(x + r * 3 / 4 + x0, y + r / 2), new Point(x + r + x0, y+r/2));//2
            ph.DrawLine(pn, new Point(x + r / 2, y + 3 * r / 4 + x0), new Point(x + r / 2, y + r + x0));//3
           ph.DrawLine(pn, new Point(x-x0,y+r/2), new Point(x+r/3-x0,y+r/2));//4
            ph.DrawEllipse(pn, x, y, r, r);
            ph.DrawEllipse(pn, x + r / 2, y + r / 2, 1, 1);
            for (int i = 0; i < 100; i++)
                ph.DrawRectangle(pn,massiv[i,0],massiv[i,1],1,1);
          }
          catch
          { }
           
        }
  static void decrace()
        {
            while (true)
            {
                if (x0 > 0)
                {
                   x0--;
                    draw();
                    
                }
                Thread.Sleep(50);
            }
        }
        
            

      
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {


            draw();

            
        }
     
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
           
           
            atack.Resume();
            shots.Resume();
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            atack.Suspend();
            shots.Suspend();
        }

     
        

    }
}
