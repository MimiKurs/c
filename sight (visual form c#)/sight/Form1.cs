﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace sight
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static int xR=0;
        static int yR=0;
        static Pen pn;
        static Brush br;
        static double mu = 0;
        static double sigma = 0.46;
        static double xstep = 0.04;
        static double mustep=0.09;
        static double jotchik;
        static bool fuze1 = true;
        static bool fuze2 = true;
        static Graphics ph;
        static Random rnd = new Random();
        static int x, y,r=25,x0=0;
        Thread atack = new Thread(attc);
        Thread decr = new Thread(decrace);
        Thread shots = new Thread(shot);
        static int[,] massiv=new int[100,2];
        static double[,] mas = new double[50, 3];
       
        private void Form1_Load(object sender, EventArgs e)
        {  
        //----- points
     
            //---points end
            ph = this.CreateGraphics();
            pn = new Pen(Color.Black, 1);
            br = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            jotchik=0;
           
            DoubleBuffered = true;
            this.Cursor.Dispose();
            atack.Start();
            atack.Suspend();
            shots.Start();
            shots.Suspend();
            decr.Start();
            for (int i = 0; i < 100; i++)
                for (int j = 0; j < 2; j++)
                    massiv[i, j] = 1;
          
        }

        static double f(double i)
        {
            
            double result = (1 / (sigma * Math.Sqrt(2 * Math.PI))) * (Math.Pow(Math.E, ((-1 * Math.Pow((i - mu), 2)) / (2 * Math.Pow(sigma, 2)))));

            return result;
        }

        static void attc()
        {
           
            while (true)
            {
               
                if (x0 < 25)
                {
                    
                    x0++;
                    mu += mustep;
                    draw();
                 
                   
                }
                Thread.Sleep(10);
            }

        }

        static void koord(bool iks, bool igryk)
        {
            do
            {
                for (int i = 0; i < 25 + x0; i++)
                {

                    double zuy = f(mas[i, 1]);

                    double random = rnd.NextDouble();
                    if (random < zuy && fuze1)
                    {
                        xR = Convert.ToInt16(mas[i, 0]);
                        fuze1 = false;
                    }

                    double random1 = rnd.NextDouble();
                    if (random1 < zuy && fuze2)
                    {
                        yR = Convert.ToInt16(mas[i, 2]);
                        fuze2 = false;
                    }
                }



            } while (fuze1 && fuze2);
            if (iks && igryk)
            {
                xR -= (x0 * 2);
                yR -= (x0 * 2);
            }
           else if (iks)
                xR -= (x0*2);
            else if (igryk)
                yR -= (x0*2);
           
        
        }

        static void shot()
        {
            try
            {
                do
                {
               int funcrandom = rnd.Next(1,5);
               if (funcrandom == 1)
                   koord(false,false);
               else if (funcrandom == 2)
                   koord(true, false);
               else if (funcrandom == 3)
                   koord(false, true);
               else if (funcrandom == 4)
                   koord(true,true);

                   // int xR = rnd.Next(x-x0, x + r+x0);
                    // int yR = rnd.Next(y-x0, y + r+x0);
                   

                    fuze1 = true;
                    fuze2 = true;

                    massiv[0, 0] = xR;
                    massiv[0, 1] = yR;
                    for (int i = 98; i > 0; i--)
                        for (int j = 0; j < 2; j++)
                        { massiv[i, j] = massiv[i - 1, j]; }
                    Thread.Sleep(100);
                } while (true);

        }
        catch
        { }
        }
         

        static void draw()
        {
           

          try {
              x = Cursor.Position.X;
              y = Cursor.Position.Y;
              xstep=1/(25+x0);
              for (int i = 0; i < 25+x0; i++)
              {
                  mas[i, 1] = jotchik;
                  jotchik += xstep;
              }

              for (int i = 0; i < 25+x0; i++)
              {
                  mas[i, 0] = x + r / 2 + i+x0;

              }
              for (int i = 0; i < 25+x0; i++)
              {
                  mas[i, 2] = y + r / 2 + i+x0;

              }

            ph.Clear(Color.White);

          
          
           // pt[0] = ; //1
         
            ph.DrawLine(pn, new Point(x+r/2,y-x0),new Point(x+r/2,y+r/4-x0)); //1
            ph.DrawLine(pn, new Point(x + r * 3 / 4 + x0, y + r / 2), new Point(x + r + x0, y+r/2));//2
            ph.DrawLine(pn, new Point(x + r / 2, y + 3 * r / 4 + x0), new Point(x + r / 2, y + r + x0));//3
           ph.DrawLine(pn, new Point(x-x0,y+r/2), new Point(x+r/3-x0,y+r/2));//4
            ph.DrawEllipse(pn, x, y, r, r);
            ph.DrawEllipse(pn, x + r / 2, y + r / 2, 1, 1);
            for (int i = 0; i < 100; i++)
                ph.DrawRectangle(pn,massiv[i,0],massiv[i,1],1,1);
          }
          catch
          { }
           
        }
  static void decrace()
        {
            while (true)
            {
                if (x0 > 0)
                {
                   x0--;
                   mu -= mustep;
                    draw();
                    
                }
                Thread.Sleep(50);
            }
        }
        
            

      
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            

            draw();
           

            
        }
     
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
           
           
            atack.Resume();
            shots.Resume();
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            atack.Suspend();
            shots.Suspend();
        }

     
        

    }
}
