﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Remember
{
    class Serialize
    {
        static public void To(string way, bool binary, Write group)
        {
            try
            {
                if (binary)
                { //binary serialize
                    using (FileStream fs = new FileStream(way, FileMode.Create))
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Serialize(fs, group);
                        fs.Close();
                    }




                }
                else
                { //now file will be situated at project/bin/debug near %program%.exe
                    XmlSerializer xmlSerializer = new XmlSerializer(group.GetType());
                    FileStream fsi = new FileStream(way, FileMode.Create);
                    xmlSerializer.Serialize(fsi, group);
                    fsi.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in Serialization procedure \n\n\n\n" + Convert.ToString(ex) + "\n\n\n\n\n");
            }


        }
        static public void Out(string way, bool binary, ref Write group)
        {
            try
            {
                //binaryDeserialize
                if (binary)
                {
                    BinaryFormatter bfb = new BinaryFormatter();
                    Write binaryTest = new Write();
                    FileStream fsI = new FileStream(way, FileMode.Open);
                    group = (Write)bfb.Deserialize(fsI);
                    fsI.Close();
                }
                else
                {
                    //xmlDeserialize
                    XmlSerializer xmlSer = new XmlSerializer(group.GetType());
                    Write xmlTest = new Write();
                    FileStream fsII = new FileStream(way, FileMode.Open);
                    group = (Write)xmlSer.Deserialize(fsII);
                    fsII.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in Deserialization procedure \n\n\n\n" + Convert.ToString(ex) + "\n\n\n\n\n");
            }


        }

    }
}
