﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Class_game
{
    class Program
    {
        public static class class1
        {
            public static void DoWork(class2 c)
            {
                c.DoWork();
            }
        }
        public class class2
        {
            public virtual void DoWork()
            {
                Console.WriteLine("class2 is working!");
            }
        }
        class class2dot1 : class2
        {
            public override void DoWork()
            {
                Console.WriteLine("class2.1 is working");
            }
        }
        class class2dot2 : class2
        {
            public override void DoWork()
            {
                Console.WriteLine("class2.2 is working");
            }
        }
        class class2dot3 : class2
        {
            public override void DoWork()
            {
                Console.WriteLine("class2.3 is working");
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Write cls name(class2,class2dot1,class2dot2,class2dot3)");
            string name = Console.ReadLine();
            if (name == "1" || name == "class2")
            {
                class2 alter = new class2();
                class1.DoWork(alter);
            }
            else if (name == "2" || name == "class2dot1")
            {
                class2dot1 alter = new class2dot1();
                class1.DoWork(alter);
            }
            else if (name == "3" || name == "class2dot2")
            {
                class2dot2 alter = new class2dot2();
                class1.DoWork(alter);
            }
            else if (name == "4" || name == "class2dot3")
            {
                class2dot3 alter = new class2dot3();
                class1.DoWork(alter);
            }
            else
                Console.WriteLine("dont find class");




            Console.ReadLine();

        }
    }
}
