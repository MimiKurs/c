﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace da4i_1._0
{                                                    //[Serializable]
    class Months
    {
        public Months()
        { }
        public string Month { get; set; }
        public string Date { get; set; }
        public double LastCount { get; set; }
        public double NowCount { get; set; }
        public double Difference { get; set; }
        public double Cash { get; set; }
        public double MembersCash { get; set; }
                                                      //[NonSerialized]
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6}", Month, Date, LastCount, NowCount, Difference, Cash, MembersCash);
        }


    }
}
