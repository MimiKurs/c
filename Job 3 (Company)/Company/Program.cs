﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        class Company
        {
            public class Employee
            {
                public string name;
                public string surname;
                public int age;

            }
            public class Director : Employee
            {
                public Director()
                {
                    name = "";
                    surname = "";
                    age = 0;
                }
                public Director(string buffName, string buffSurname, int buffAge)
                {
                    name = buffName;
                    surname = buffSurname;
                    age = buffAge;
                }
                public int numManagers;
                public string createXml()
                {
                    string result;
                    return result = "<Director: name=" + name + " surname=" + surname + " age=" + age + " > ";
                }

            }
            public class Manager : Employee
            {
                public Manager()
                {
                    name = "";
                    surname = "";
                    age = 0;
                }
                public Manager(string buffName, string buffSurname, int buffAge)
                {
                    name = buffName;
                    surname = buffSurname;
                    age = buffAge;
                }
                public int numWorkers;
                public string createXml()
                {
                    string result;
                    return result = "<Manager: name=" + name + " surname=" + surname + " age=" + age + " > ";
                }

            }

            public class Workers : Employee
            {
                public Workers()
                {
                    name = "";
                    surname = "";
                    age = 0;
                }
                public Workers(string buffName, string buffSurname, int buffAge)
                {
                    name = buffName;
                    surname = buffSurname;
                    age = buffAge;
                }
                public int id;
                public string createXml()
                {
                    string result;
                    return result = "<Worker: name=" + name + " surname=" + surname + " age=" + age + " > ";
                }
            }
        }
        static void Main(string[] args)
        {
            int managerCount, workerCount;
            Console.WriteLine("Введите кол-во менеджеров");
            managerCount = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Введите кол-во рабочих");
            workerCount = Convert.ToInt16(Console.ReadLine());
            Company.Director[] directors = new Company.Director[1];
            Company.Manager[] managers = new Company.Manager[managerCount];
            Company.Workers[] workers = new Company.Workers[workerCount];

            for (int i = 0; i < 1; i++)
            {
                directors[i] = new Company.Director();
                directors[i].numManagers = managerCount;
                Console.WriteLine("Введите имя директора-" + (i + 1));
                directors[i].name = Console.ReadLine();
                Console.WriteLine("Введите фамилию директора-" + (i + 1));
                directors[i].surname = Console.ReadLine();
                Console.WriteLine("Введите возраст директора-" + (i + 1));
                directors[i].age = Convert.ToInt16(Console.ReadLine());

            }

            for (int i = 0; i < managerCount; i++)
            {
                managers[i] = new Company.Manager();
                managers[i].numWorkers = workerCount;
                Console.WriteLine("Введите имя менеджера-" + i + 1);
                managers[i].name = Console.ReadLine();
                Console.WriteLine("Введите фамилию менеджера-" + (i + 1));
                managers[i].surname = Console.ReadLine();
                Console.WriteLine("Введите возраст менеджера-" + (i + 1));
                managers[i].age = Convert.ToInt16(Console.ReadLine());

            }

            for (int i = 0; i < workerCount; i++)
            {
                workers[i] = new Company.Workers();
                workers[i].id = i;
                Console.WriteLine("Введите имя рабочего-" + (i + 1));

                workers[i].name = Console.ReadLine();
                Console.WriteLine("Введите фамилию рабочего-" + (i + 1));
                workers[i].surname = Console.ReadLine();
                Console.WriteLine("Введите возраст рабочего-" + (i + 1));
                workers[i].age = Convert.ToInt16(Console.ReadLine());

            }

            StreamWriter sr = new StreamWriter("Company.txt");
            sr.WriteLine("<company> ");
            sr.WriteLine("<employees> ");
            for (int i = 0; i < 1; i++)
            {

                sr.WriteLine(directors[i].createXml());

            }
            for (int i = 0; i < managerCount; i++)
            {

                sr.WriteLine(managers[i].createXml());

            }
            for (int i = 0; i < workerCount; i++)
            {

                sr.WriteLine(workers[i].createXml());

            }
            sr.WriteLine("</employees> ");
            sr.WriteLine("</company> ");
            sr.Close();
            Console.WriteLine("Ok!");
            Console.ReadLine();
        }
    }
}
