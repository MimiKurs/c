﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Calc;

namespace Calc_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("( ) ^ sqrt not supported yet...");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try {

                MessageBox.Show(string.Concat("=",Convert.ToString(Parse.ParseThis(expression.Text))));
            }
            catch 
            {
                MessageBox.Show("ошибка");
            }
        }
    }
}
