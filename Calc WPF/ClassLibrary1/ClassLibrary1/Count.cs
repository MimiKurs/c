﻿
namespace Calc
{
    static public class Count
    {
        static public int CountThis(string source, char symbol)
        {
            int result = 0;
            int countFrom = 0;
            while (source.IndexOf(symbol, countFrom) != -1)
            {
                countFrom = source.IndexOf(symbol, countFrom) + 1;
                result++;

            }


            return result;
        }
    }
}
