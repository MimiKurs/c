﻿using System;
using System.Collections.Generic;


namespace Calc
{
    public static class Parse
    {
     
        public static double ParseThis(string data)
        {
            try
            {
                int length = Count.CountThis(data, '+') + Count.CountThis(data, '-') + Count.CountThis(data, '*') + Count.CountThis(data, '/') + 1;

                List<string> massiv = new List<string>();
                #region split massiv



                //Create massiv 
                data = data.Replace("*", " * ");
                data = data.Replace("/", " / ");
                data = data.Replace("-", " - ");
                data = data.Replace("+", " + ");

                string[] TempCap = data.Split(' ');

                for (var i = 0; i < TempCap.Length; i++)
                    massiv.Add(TempCap[i]);

                bool signB = false;
                while (!signB)
                {
                    signB = true;
                    for (var i = 0; i < massiv.Count; i++)
                    {
                        if (massiv[i] == "+" || massiv[i] == "-" || massiv[i] == "/" || massiv[i] == "*")
                        {
                            massiv[i - 1] += massiv[i];
                            massiv.RemoveAt(i);
                            signB = false;
                        }
                    }
                }

                #endregion
                #region evaluate
                //begin search * and / and evaluate it
                int schetchik = 1;
                while (schetchik != 0)
                {
                    schetchik = 0;
                    for (var i = 0; i < massiv.Count; i++)
                        if (massiv[i][massiv[i].Length - 1] == '/')
                        {
                            string sign = "";


                            double b1;

                            if (i != massiv.Count - 2)
                            {
                                sign = Convert.ToString(massiv[i + 1][massiv[i + 1].Length - 1]);
                                massiv[i + 1] = massiv[i + 1].Remove(massiv[i + 1].Length - 1, 1);
                                b1 = Convert.ToDouble(massiv[i + 1]);
                            }
                            else
                                b1 = Convert.ToDouble(massiv[i + 1]);

                            massiv[i] = massiv[i].Remove(massiv[i].Length - 1, 1);
                            double a1 = Convert.ToDouble(massiv[i]);
                            massiv.RemoveAt(i + 1);


                            massiv[i] = Convert.ToString(a1 / b1) + sign;

                            schetchik++;
                        }
                        else if (massiv[i][massiv[i].Length - 1] == '*')
                        {
                            string sign = "";


                            double b1;

                            if (i != massiv.Count - 2)
                            {
                                sign = Convert.ToString(massiv[i + 1][massiv[i + 1].Length - 1]);
                                massiv[i + 1] = massiv[i + 1].Remove(massiv[i + 1].Length - 1, 1);
                                b1 = Convert.ToDouble(massiv[i + 1]);
                            }
                            else
                                b1 = Convert.ToDouble(massiv[i + 1]);

                            massiv[i] = massiv[i].Remove(massiv[i].Length - 1, 1);
                            double a1 = Convert.ToDouble(massiv[i]);
                            massiv.RemoveAt(i + 1);


                            massiv[i] = Convert.ToString(a1 * b1) + sign;
                            schetchik++;

                        }
                }


                while (massiv.Count != 1)
                {
                    for (var i = 0; i < massiv.Count; i++)
                        if (massiv[i][massiv[i].Length - 1] == '+')
                        {
                            string sign = "";


                            double b1;

                            if (i != massiv.Count - 2)
                            {
                                sign = Convert.ToString(massiv[i + 1][massiv[i + 1].Length - 1]);
                                massiv[i + 1] = massiv[i + 1].Remove(massiv[i + 1].Length - 1, 1);
                                b1 = Convert.ToDouble(massiv[i + 1]);
                            }
                            else
                                b1 = Convert.ToDouble(massiv[i + 1]);

                            massiv[i] = massiv[i].Remove(massiv[i].Length - 1, 1);
                            double a1 = Convert.ToDouble(massiv[i]);
                            massiv.RemoveAt(i + 1);


                            massiv[i] = Convert.ToString(a1 + b1) + sign;


                        }

                        else if (massiv[i][massiv[i].Length - 1] == '-')
                        {
                            string sign = "";


                            double b1;

                            if (i != massiv.Count - 2)
                            {
                                sign = Convert.ToString(massiv[i + 1][massiv[i + 1].Length - 1]);
                                massiv[i + 1] = massiv[i + 1].Remove(massiv[i + 1].Length - 1, 1);
                                b1 = Convert.ToDouble(massiv[i + 1]);
                            }
                            else
                                b1 = Convert.ToDouble(massiv[i + 1]);

                            massiv[i] = massiv[i].Remove(massiv[i].Length - 1, 1);
                            double a1 = Convert.ToDouble(massiv[i]);
                            massiv.RemoveAt(i + 1);


                            massiv[i] = Convert.ToString(a1 - b1) + sign;


                        }


                }



                #endregion

                //result
                return Convert.ToDouble(massiv[0]);
            }
            catch
            { }
    
        }
    }
}

